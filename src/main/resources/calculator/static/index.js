'use strict';
((D, B, log = arg => console.log(arg)) => {
    let sign = '', mr = 0, a = 0, sign2 = '', k = D.querySelector('[type=text]');
    const mAct = {
        '+':(a, b) => { k.value = a + b; },
        '-':(a, b) => { k.value = a - b; },
        '*':(a, b) => { k.value = a * b; },
        '/':(a, b) => { k.value = a / b; },
      'mrc':() => { (mr !== 0 && k.value === '0') ? k.value = mr : mr = 0 },  //якщо на екрані 0 повертає пам'ять на екран інакше стирає
       'm-':() => { mr = +mr - +k.value },      //віднімає від пам'яті
       'm+':() => { mr = +mr + +k.value },      //додає до пам'яті
    },
        write = e =>{ let l = k.value; k.value = `${l}${e.target.value}` },
        concat = (e) => {
        if(e.target.value !== undefined && e.target.value.match(/^\d|[.]$/)){
        if(k.value === '0' && e.target.value === '.'){ k.value = `${k.value}${e.target.value}` }
        if(k.value === '0'){ k.value = '' }
        if (!k.value.includes('.') && e.target.value === '.'){ write(e) }
               if(e.target.value !== '.'){ write(e) }
          }
        },
        swap = e => { if(e.target.value !== undefined && e.target.value.match(/^\d|[.]$/) && sign !== ''){ k.value = ''; sign2 = sign; sign = '' } },
        clean = e => { if (e.target.value === 'C'){ k.value = '0' } },
        read = e => { if (e.target.value !== undefined && !(e.target.value.match(/^\d|[.]$/)) && e.target.value !== 'C'){ sign = e.target.value; a = k.value; } },
        result = e => { if (sign2 !== undefined && sign2 !== '' && e.target.value === '=' && a !== 0 ){ mAct[sign2](+a, +k.value) } },
        memorize = e => { if (e.target.value !== undefined && e.target.value.match(/^mrc|m-|m\+$/)){ mAct[e.target.value]() } },
        m = () => {(mr === 0) ? D.querySelector('.mr').textContent = '' : D.querySelector('.mr').textContent = 'm'};

     D.querySelector('.box').addEventListener('click',(e)=>{
         result(e); memorize(e); swap(e); clean(e); read(e); concat(e); m(e)
     })

    log('the end')

})(document, document.body);