package project;

import jakarta.servlet.ServletException;
import jakarta.servlet.ServletOutputStream;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

@WebServlet
public class StaticContentServlet extends HttpServlet {
    private final String osStaticLocation;
    public StaticContentServlet(String osStaticLocation) {
        this.osStaticLocation = osStaticLocation;
    }
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String pathInfo = req.getPathInfo().substring(1);
        System.out.println(pathInfo);
        String extension = pathInfo.split("\\.")[1];

        switch (extension){
            case "css" -> resp.setContentType("text/css");
            case "js" -> resp.setContentType("text/javascript");
            case "jpeg" -> resp.setContentType("image/jpeg");
            case "svg" -> resp.setContentType("image/svg+xml");
            case "png" -> resp.setContentType("image/png");
        }

        Path file = Path.of(osStaticLocation, pathInfo);

        if (!file.toFile().exists()) {
            resp.setStatus(404);
        } else {
            try (ServletOutputStream os = resp.getOutputStream()) {
                Files.copy(file, os);
            }
        }
    }
}
