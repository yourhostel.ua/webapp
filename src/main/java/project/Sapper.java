package project;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;


@WebServlet
public class Sapper extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String uri = ResourcesOps.dirUnsafe("sapper/index.html");
        Path fileWithFullPath = Paths.get(uri);

        try (PrintWriter w = resp.getWriter()) {
            Files
                    .readAllLines(fileWithFullPath)
                    .forEach(w::println);
        }
    }
}
