package project;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class HelloServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String uri = ResourcesOps.dirUnsafe("home/index.html");
        Path fileWithFullPath = Paths.get(uri);

        try (PrintWriter w = resp.getWriter()) {
            Files
                    .readAllLines(fileWithFullPath)
                    .forEach(w::println);
        }
    }
}
