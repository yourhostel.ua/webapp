package project;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;

import java.net.InetSocketAddress;

public class Main {
    public static void main(String[] args) throws Exception {
        InetSocketAddress inetSocketAddress = new InetSocketAddress(" " , 0);
        Server server = new Server(inetSocketAddress);

        ServletContextHandler handler = new ServletContextHandler();

        handler.addServlet(new ServletHolder(new StaticContentServlet(ResourcesOps.dirUnsafe("calculator/static"))), "/calculator/static/*");
        handler.addServlet(new ServletHolder(new StaticContentServlet(ResourcesOps.dirUnsafe("sapper/static"))), "/sapper/static/*");
        handler.addServlet(new ServletHolder(new HelloServlet()),"/");
        handler.addServlet(new ServletHolder(new Calculator()), "/calculator");
        handler.addServlet(new ServletHolder(new Sapper()), "/sapper");
        server.setHandler(handler);
        server.start();
        server.join();
    }
}
