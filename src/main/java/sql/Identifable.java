package sql;

public interface Identifable {
    Integer id();
}
