package sql;

import java.sql.*;
import java.util.Optional;

public class ConnectionApp {
    public static void main(String[] args) throws Exception {
        Connection conn = DriverManager.getConnection(
                "jdbc:postgresql://localhost:5432/test",
                "postgres",
                "52486"
        );

        insert(new User(1, "Tim"), conn);
        //delete(4, conn);
        System.out.println(load(1, conn));
    }

    public static void insert(User user, Connection conn) throws Exception{
        PreparedStatement stmt = conn.prepareStatement("INSERT INTO users (name) values (?)");
        stmt.setString(1, user.name());
        stmt.execute();
    }

    private static void update(User user, Connection conn) throws Exception{
        PreparedStatement stmt = conn.prepareStatement("UPDATE users SET name = ? WHERE id = ?");
        stmt.setString(1, user.name());
        stmt.setInt(2, user.id());
        stmt.execute();
    }

    public static void save(User user, Connection conn ) throws Exception {
        if (user.id() == null) insert(user, conn);
        else                   update(user, conn);
    }

    public static Optional<User> load(int id, Connection conn) throws Exception {
        PreparedStatement stmt = conn.prepareStatement("SELECT id, name FROM users WHERE id = ?");
        stmt.setInt(1, id);
        // 0 or 1 record
        ResultSet rs = stmt.executeQuery();
        if (rs.next()) {
            return Optional.of(new User(
                    rs.getInt("id"),
                    rs.getString("name")
            ));
        }
        return Optional.empty();
    }

    public static void delete(int id, Connection conn) throws Exception {
        PreparedStatement stmt = conn.prepareStatement("DELETE FROM users WHERE id = ?");
        stmt.setInt(1, id);
        stmt.executeUpdate();
    }
}
